# Public Code Piveau

Dieses Repository enthält die Datei publiccode.yml, die für die Veröffentlichung im Softwareverzeichnis von opencode.de benötigt wird

# Code

piveau hat eine Architektur mit verteilten Diensten. Das bedeutet, dass die Plattform aus mehreren Repositorys besteht, von denen jedes den Code eines Dienstes enthält. Zu den Codes der einzelnen Dienste gelangen Sie bitte über die folgende Gruppe.

https://gitlab.opencode.de/piveau

# General

piveau ist eine Datenmanagement-Plattform für den öffentlichen Sektor. Es kann für den flexiblen Aufbau von leistungsstarken, offenen Datenportalen oder internen Datenmanagementsystemen eingesetzt werden. Durch die Verwendung von offenen Standards und Cloud-Technologien lässt sich piveau einfach in eine bestehende Umgebung integrieren und zusammen mit bereits eingesetzten Anwendungen verwenden.


## Flexibilität

Durch die DataFlow Prozessengine lässt piveau besonders leicht an neue Anforderungen anpassen.

## Offene Standards

piveau setzt konsequent auf offene Standards und hohe Interoperabilität.

## Moderne Architektur

Durch den Einsatz moderner Architekturprinzipien ist piveau bestens für die Zukunft vorbereitet.

# Weitere Infos

https://www.piveau.eu/


---


# Public code piveau

This repository contains the publiccode.yml file, which is required for publication in the opencode.de software directory

# code

piveau has a distributed services architecture. This means that the platform consists of multiple repositories, each containing a service's code. Please use the following group to access the codes for the individual services.

https://gitlab.opencode.de/piveau

#General

piveau is a data management platform for the public sector. It can be used for the flexible construction of powerful, open data portals or internal data management systems. By using open standards and cloud technologies, piveau can be easily integrated into an existing environment and used together with applications that are already in use.


## Flexibility

Thanks to the DataFlow process engine, piveau can be easily adapted to new requirements.

## Open standards

piveau consistently relies on open standards and high interoperability.

## Modern architecture

By using modern architectural principles, piveau is well prepared for the future.

# Further information

https://www.piveau.eu/
